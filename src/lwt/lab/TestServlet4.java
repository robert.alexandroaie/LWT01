package lwt.lab;

import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lwt.lab.html.utils.HTMLUtils;

/**
 * Servlet implementation class TestServlet4
 */
@WebServlet("/test4")
public class TestServlet4 extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static final String MAJOR_CHANGES = "Major changes:";
	public static final String DATE_PATTERN = "yyyy/mm/dd";

	/**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet4() {
	super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.getWriter().append(HTMLUtils.titledHTML("Test4"));
	response.getWriter().append("Served at: ").append(request.getContextPath());


		System.out.println(MAJOR_CHANGES);

		LocalDateTime dateTime = LocalDateTime.now();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_PATTERN);



		System.out.println(dateTime.format(dtf));

		Random random = new Random();
		random.doGet(request,response);

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	doGet(request, response);
    }

}
